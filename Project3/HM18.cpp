﻿#include <iostream>
#include <string>

using namespace std;

class Animal
{
public:
	virtual void Voice()
	{
		cout << "Animal Class" << "\n";
	}
	virtual ~Animal(){}

};

class Dog : public Animal
{
public:
	void Voice() override
	{
		cout << "Gaf!" << "\n";
	}

};

class Cat : public Animal
{
public:
	void Voice() override
	{
		cout << "Mayyy!" << "\n";
	}
};

class Snake : public Animal
{
public:
	void Voice() override
	{
		cout << "Shshshshsh!" << "\n";
	}

};

int main()
{
	Animal* arr[3]{ new Dog, new Cat, new Snake };

	for (auto i : arr)
	{
		i->Voice();
		delete i;
	}
	system("pause");
}
